﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using Tesseract;
using WindowsFormsApplication1;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int mouseclick = 0, CursorX1 = 24, CursorX2 = 470, CursorY1 = 730, CursorY2 = 1010;

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {

        }

        Form2 form2 = new Form2();

        private void rePoint()
        {
            this.Width = CursorX2 - CursorX1 + pictureBox1.Width;
            this.Height = CursorY2 - CursorY1 + textBox1.Height + 5; if (this.Height < 120) this.Height = 120;
            this.Location = new Point(CursorX1, CursorY1);
            button1.Location = new Point(this.Width - button1.Width / 2 - pictureBox1.Width / 2, 0);
            pictureBox1.Location = new Point(this.Width - pictureBox1.Width, button1.Height + 10);
            pictureBox2.Location = new Point(this.Width - pictureBox2.Width, pictureBox1.Location.Y + pictureBox1.Height + 10);
            button2.Location = new Point(this.Width - button2.Width / 2 - pictureBox2.Width / 2, pictureBox2.Location.Y + button2.Height + 10);
            textBox1.Location = new Point(0, this.Height - textBox1.Height);
            textBox1.Width = CursorX2 - CursorX1;
            form2.richTextBox1.Width = form2.Width = this.Width;
            form2.richTextBox1.Height = form2.Height = this.Height;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            CursorX1 = WindowsFormsApp1.Properties.Settings.Default.CursorX1;
            CursorX2 = WindowsFormsApp1.Properties.Settings.Default.CursorX2;
            CursorY1 = WindowsFormsApp1.Properties.Settings.Default.CursorY1;
            CursorY2 = WindowsFormsApp1.Properties.Settings.Default.CursorY2;
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatStyle = FlatStyle.Flat;
            button2.FlatAppearance.BorderSize = 0;
            button2.FlatStyle = FlatStyle.Flat;
            rePoint();
            form2.richTextBox1.BorderStyle = BorderStyle.None;            
        }

        private string ocr(Bitmap printscreen)
        {
            var ocrtext = string.Empty;
            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                using (var img = PixConverter.ToPix(printscreen))
                {
                    using (var page = engine.Process(img))
                    {
                        ocrtext = page.GetText();
                    }
                }
            }

            return ocrtext;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_Leave(object sender, EventArgs e)
        {

        }


        private static ushort key(string name)
        {
            var dict = new Dictionary<string, ushort>
            {{"`",0x29},{"1",0x02},{"2",0x03 },{"3",0x04 },{"4",0x05 },{"5",0x06 },{"6",0x07 },{"7",0x08 },{"8",0x09 },{"9",0x0A },{"0",0x0B },{"-",0x0C },{"=",0x0D },{"Tab",0x0F },{"q",0x10 },{"w",0x11 },{"e",0x12 },{"r",0x13 },{"t",0x14 },{"y",0x15 },{"u",0x16 },{"i",0x17 },{"o",0x18 },{"p",0x19 },{"[",0x1A },{"]",0x1B },{"a",0x1E },{"s",0x1F },{"d",0x20 },{"f",0x21 },{"g",0x22 },{"h",0x23 },{"j",0x24 },{"k",0x25 },{"l",0x26 },{";",0x27 },{"'",0x28 },{"\\",0x2B},{"z",0x2C },{"x",0x2D },{"c",0x2E },{"v",0x2F },{"b",0x30 },{"n",0x31 },{"m",0x32 },{",",0x33 },{".",0x34 },{"/",0x35 },{"f1",0x3b },{"f2",0x3c },{"f3",0x3d },{"f4",0x3e },{"f5",0x3f },{"f6",0x40 },{"f7",0x41 },{"f8",0x42 },{"f9",0x43 },{"f10",0x44 },{"f11",0x57 },{"f12",0x58 },{"Scroll lock",0x46 },{"esc",0x01 },{"return",0x1c },{"enter",0x1c },{"space",0x39 },{" ",0x39 },{"insert",0xd2 },{"delete",0xd3 },{"backspace",0x0e },{"left",0xcb },{"right",0xcd },{"up",0xc8 },{"down",0xd0 },{"home",0xc7 },{"end",0xcf },{"page up",0xc9 },{"page down",0xd1 },{"Numlock",0x45 },{"Num_=,",0xd },{"Num_/",0xb5 },{"Num_*",0x37 },{"Num_-",0x4a },{"Num_+",0x4e },{"Num_.",0x53 },{"Num_0",0x52 },{"Num_1",0x4f },{"Num_2",0x50 },{"Num_3",0x51 },{"Num_4",0x4b },{"Num_5",0x4c },{"Num_6",0x4d },{"Num_7",0x47 },{"Num_8",0x48 },{"Num_9",0x49 },{"Num_enter",0x9c },{"а", 0x21 },{"б", 0x33 },{"в", 0x20 },{"г", 0x16 },{"д", 0x26 },{"е", 0x14 },{"ё", 0x29},{"ж", 0x27 },{"з", 0x19 },{"й", 0x10 },{"и", 0x30 },{"к", 0x13 },{"л", 0x25 },{"м", 0x2F },{"н", 0x15 },{"о", 0x24 },{"п", 0x22 },{"р", 0x23 },{"с", 0x2E },{"т", 0x31 },{"у", 0x12 },{"ф", 0x1E},{"х", 0x1A },{"ц", 0x11 },{"ч", 0x2D },{"ш", 0x17 },{"щ", 0x18 },{"ъ", 0x1B },{"ы", 0x1F },{"ь", 0x32 },{"э", 0x28 },{"ю", 0x34 },{"я", 0x2C }};

            return dict[name];
        }


        public static Keys ConvertFromString(string keystr)
        {
            return (Keys)Enum.Parse(typeof(Keys), keystr);
        }
        public void send(string name_of_key) { send_down(name_of_key); Thread.Sleep(100); send_up(name_of_key); }
        public void send_down(string name_of_key) { WindowsFormsApplication1.KeyBoardSimulator.SendKey(key(name_of_key), WindowsFormsApplication1.KeyBoardSimulator.KEYEVENTF.KeyDown | WindowsFormsApplication1.KeyBoardSimulator.KEYEVENTF.SCANCODE); }
        public void send_up(string name_of_key) { WindowsFormsApplication1.KeyBoardSimulator.SendKey(key(name_of_key), WindowsFormsApplication1.KeyBoardSimulator.KEYEVENTF.KEYUP | WindowsFormsApplication1.KeyBoardSimulator.KEYEVENTF.SCANCODE); }
        public void wsend(string name_of_key) { wsend_down(name_of_key); Thread.Sleep(100); wsend_up(name_of_key); }
        public void wsend_down(string name_of_key) { WindowsFormsApplication1.KeyBoardSimulator.KeyboardSend.KeyDown(ConvertFromString(name_of_key.Replace(" ", ""))); }
        public void wsend_up(string name_of_key) { KeyBoardSimulator.KeyboardSend.KeyUp(ConvertFromString(name_of_key.Replace(" ", ""))); }
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Clipboard.SetDataObject(Translate(textBox1.Text, "ru", "en"));
                //wsend_down("LMenu"); send("Tab"); wsend_up("LMenu"); Thread.Sleep(10);
                //send("enter"); wsend_down("ControlKey"); send("v"); wsend_up("ControlKey"); send("enter");
                textBox1.Text = ""; textBox1.Visible = false;
            }
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                send("Tab");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (!form2.Visible)
            {
                Bitmap printscreen = new Bitmap(CursorX2 - CursorX1, CursorY2 - CursorY1);
                Graphics graphics = Graphics.FromImage(printscreen as Image);
                Size s = new Size(CursorX2 - CursorX1, CursorY2 - CursorY1);
                graphics.CopyFromScreen(CursorX1, CursorY1, 0, 0, s);
                form2.richTextBox1.Text = Translate(ocr(printscreen).Replace("\n\n", "\n"), "auto", "ru");
                form2.Visible = true;
                timer1.Enabled = true;
                form2.BackgroundImage = printscreen;
            }
            else { form2.Visible = false; timer1.Enabled = false; }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            form2.Visible = false;
            timer1.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            WindowsFormsApp1.Properties.Settings.Default.CursorX1 = CursorX1;
             WindowsFormsApp1.Properties.Settings.Default.CursorX2 = CursorX2;
            WindowsFormsApp1.Properties.Settings.Default.CursorY1 = CursorY1;
            WindowsFormsApp1.Properties.Settings.Default.CursorY2 = CursorY2;
            WindowsFormsApp1.Properties.Settings.Default.Save();
        }

        private void button2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.A)
            {
                if (mouseclick == 0)
                {
                    CursorX1 = Cursor.Position.X;
                    CursorY1 = Cursor.Position.Y;
                }

                if (mouseclick == 1)
                {
                    CursorX2 = Cursor.Position.X;
                    CursorY2 = Cursor.Position.Y;
                    if (CursorX1 > CursorX2) { int temp = CursorX1; CursorX1 = CursorX2; CursorX2 = temp; }
                    if (CursorY1 > CursorY2) { int temp = CursorY1; CursorY1 = CursorY2; CursorY2 = temp; }
                    rePoint();
                    //this.Opacity = 1;
                }
                mouseclick = mouseclick + 1;
                //if (mouseclick == 2) { label1.Text = Convert.ToString(CursorX1) + ':' + Convert.ToString(CursorY1) + "  " + Convert.ToString(CursorX2) + ':' + Convert.ToString(CursorY2); }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (textBox1.Visible) { textBox1.Visible = false; } else { textBox1.Visible = true; textBox1.Focus(); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public String Translate(String word, string fromLanguage, string toLanguage)
        {
            string text = "";
            var url = $"https://translate.googleapis.com/translate_a/single?client=gtx&sl={fromLanguage}&tl={toLanguage}&hl=ru&dt=t&dj=1&source=icon&tk=467103.467103&q={HttpUtility.UrlEncode(word)}";
            var webClient = new WebClient
            {
                Encoding = System.Text.Encoding.UTF8
            };


            var result = webClient.DownloadString(url);
            int indexof = result.IndexOf("\"trans\":\"");

            try
            {
                while (indexof != -1)
                {
                    int start = result.IndexOf("\"trans\":\"");
                    int stop = result.IndexOf("\"orig\":\"") - 2;
                    int n = ("\"trans\":\"").Length;
                    string new_str = result.Substring(start + n, stop - start - n);
                    //if ((text.Length>0) && (text.Substring(text.Length-1,1)!=" ") && (new_str.Substring(0, 1) != " ")) { text += " "; }
                    text += new_str; result = result.Remove(0, stop + n);
                    indexof = result.IndexOf("\"trans\":\"");
                }
                return text.Replace("\\n", "\n").Replace("\\u0027", "\'").Replace("\\u0026", "&").Replace("\\u003d", "=").Replace("\\u003e", ">").Replace("\\u003c", "<");
            }
            catch
            {
                return "Error";
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            mouseclick = 0;
        }
    }
}
